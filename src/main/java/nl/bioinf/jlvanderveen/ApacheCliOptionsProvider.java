package nl.bioinf.jlvanderveen;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.util.ArrayList;


/**
 * This class implements the OptionsProvider interface by parsing the passed command line arguments.
 *
 * @author michiel
 */
public class ApacheCliOptionsProvider implements OptionsProvider {
    private static final String HELP = "help";
    private static final String FILE = "file";
    private static final String INSTANCE = "instance";


    private final String[] clArguments;
    private Options options;
    public CommandLine commandLine;



    /**
     * constructs with the command line array.
     *
     * @param args the CL array
     */
    public ApacheCliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * check if help was requested; if so, return true.
     * @return helpRequested
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * builds the Options object.
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message");
        Option fileOption = new Option("f", FILE, true, "User given .arff-file which contains " +
                "instances to be classified with as default test_data.arff if a file is given it is assumed that the data" +
                " was log2-transform and that the length and diameter attributes were already merged (by multiplying them)");
        Option instanceOption = new Option("i", INSTANCE, true, "A single instance to be" +
                " classified which is comma separated (like 1,2,3,...) in the order Humerus, Ulna, Femur, Tibiotarsus, tarsometatarsus");

        options.addOption(helpOption);
        options.addOption(fileOption);
        options.addOption(instanceOption);

    }

    /**
     * processes the command line arguments.
     */
    private void processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);

            // Check for legal instances

        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex);
        }

    }
    
    /**
     * Checks the length of the instances (accepted sizes are 5 or 10)
     */
    private double[] checkLengthInstance(String[] instance){
        try {
            if ((instance.length == 5)) {
                // Can be put into getSingleInstance instantly

                Double hum = Double.parseDouble(instance[0]);
                Double ulna = Double.parseDouble(instance[1]);
                Double fem = Double.parseDouble(instance[2]);
                Double tib = Double.parseDouble(instance[3]);
                Double tar = Double.parseDouble(instance[4]);
                double[] doubles = {hum, ulna, fem, tib, tar};
                return doubles;

            } else if (instance.length == 10) {
                // Must be normalised first by taking the log2 and multiplying length by diameter

                Double hum = Math.log((Double.parseDouble(instance[0]) * Double.parseDouble(instance[1])))/Math.log(2);
                Double ulna = Math.log((Double.parseDouble(instance[2]) * Double.parseDouble(instance[3])))/Math.log(2);
                Double fem = Math.log((Double.parseDouble(instance[4]) * Double.parseDouble(instance[5])))/Math.log(2);
                Double tib = Math.log((Double.parseDouble(instance[6]) * Double.parseDouble(instance[7])))/Math.log(2);
                Double tar = Math.log((Double.parseDouble(instance[8]) * Double.parseDouble(instance[9])))/Math.log(2);
                double[] doubles = {hum, ulna, fem, tib, tar};
                return doubles;

            } else {
                // The length is not allowed and should stop the program
                // warn user that floating points should be noted with dots (1.4) instead of comma (1,4)
                System.out.println("Wrong instance length make sure floating points are noted with dots (1.4)" +
                        "instead of commas (1,4)");
                return null;
            }
        }
        catch (NumberFormatException ex){
            throw new IllegalArgumentException("The instance contains illegal characters");
        }

    }

    /**
     *  Normalise the data if length is 10
     * @param instance
     * @return
     */
    private ArrayList<Double> normaliseInstance(ArrayList<Double> instance){
         
        return instance;
    }



    /**
     * prints help.
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("MyCoolTool", options);
    }

    @Override
    public String getFileName() {
        return this.commandLine.getOptionValue(FILE, "test_data.arff");
    }

    @Override
    public double[] getSingleInstance() {
        if( this.commandLine.hasOption(INSTANCE)){
        String[] instanceString = this.commandLine.getOptionValue(INSTANCE).split(",");
        double[] instanceDouble = checkLengthInstance(instanceString);
        return instanceDouble;}
        else{ return null;}

    }

}
