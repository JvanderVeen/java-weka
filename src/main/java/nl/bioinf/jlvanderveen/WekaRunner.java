package nl.bioinf.jlvanderveen;

import weka.classifiers.meta.Stacking;
import weka.core.*;

import weka.core.converters.ConverterUtils;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;


import java.io.IOException;
import java.util.Arrays;


/**
 * If you saved a model to a file in WEKA, you can use it reading the generated java object.
 * Here is an example with Random Forest classifier (previously saved to a file in WEKA):
 * import java.io.ObjectInputStream;
 * import weka.core.Instance;
 * import weka.core.Instances;
 * import weka.core.Attribute;
 * import weka.core.FastVector;
 * import weka.classifiers.trees.RandomForest;
 * RandomForest rf = (RandomForest) (new ObjectInputStream(PATH_TO_MODEL_FILE)).readObject();
 * <p>
 * or
 * RandomTree treeClassifier = (RandomTree) SerializationHelper.read(new FileInputStream("model.weka")));
 */
public class WekaRunner {
    private final String modelFile = "stacking_randomforest.model";

    public WekaRunner(String[] args) {
        try {
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(args);
            if (op.helpRequested()) {
                op.printHelp();
                return;
            }
            start(op);
        } catch (IllegalStateException ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(new String[]{});
            op.printHelp();

        }

    }
    private Instances scale_data(String datafile){

        ConverterUtils.DataSource source;

        try {
            source = new ConverterUtils.DataSource(datafile);

            Instances data = source.getDataSet();

            return data;

        } catch (Exception e) {
            e.getMessage();

        }
        return null;
    }

    private void start(ApacheCliOptionsProvider op) {
        String datafile = "training_data.arff";
        String unknownArffFile = op.getFileName();



        try {
            Instances instances = scale_data(datafile);
            printInstances(instances);
            Stacking fromFile = loadClassifier();

            if (op.commandLine.hasOption("instance")) {
                double[] values = op.getSingleInstance();
                Instances instance = createSingle(values);

                Add filter = new Add();
                filter.setAttributeIndex("last");
                filter.setNominalLabels("SW,R,W,T,P,SO");
                filter.setAttributeName("type");
                filter.setInputFormat(instance);
                instance = Filter.useFilter(instance, filter);

                instance.setClassIndex(instance.numAttributes() - 1);
                System.out.println("newData # attributes = " + instance.numAttributes());
                System.out.println("newData class index= " + instance.classIndex());
                classifyNewInstance(fromFile, instance);

            } else {
                Instances unknownInstancesFromCsv = scale_data(unknownArffFile);
                if (unknownInstancesFromCsv.classIndex() == -1)
                    unknownInstancesFromCsv.setClassIndex(unknownInstancesFromCsv.numAttributes() - 1);

                Instances newData = new Instances(unknownInstancesFromCsv);
                // 1. nominal attribute
                Add filter = new Add();
                filter.setAttributeIndex("last");
                filter.setNominalLabels("SW,R,W,T,P,SO");
                filter.setAttributeName("type");
                filter.setInputFormat(newData);
                newData = Filter.useFilter(newData, filter);

                newData.setClassIndex(newData.numAttributes() - 1);
                System.out.println("newData # attributes = " + newData.numAttributes());
                System.out.println("newData class index= " + newData.classIndex());

                System.out.println("unknownInstancesFromCsv = " + newData);

                classifyNewInstance(fromFile, newData);
            }
            } catch(Exception e){
                e.getMessage();

        }
    }

    private Instances createSingle(double[] values){
        Attribute hum = new Attribute("volume.hum");
        Attribute ulna = new Attribute("volume.ulna");
        Attribute fem = new Attribute("volume.fem");
        Attribute tib = new Attribute("volume.tib");
        Attribute tar = new Attribute("volume.tar");

        FastVector wekafv = new FastVector();
        wekafv.addElement(hum);
        wekafv.addElement(ulna);
        wekafv.addElement(fem);
        wekafv.addElement(tib);
        wekafv.addElement(tar);

        Instances single = new Instances("singleInstance", wekafv, 0);
        DenseInstance instance = new DenseInstance(1.0, values);
        single.add(instance);
        return single;
    }

    private void classifyNewInstance(Stacking meta, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = meta.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
    }

    private Stacking loadClassifier() throws Exception {

        return (Stacking) weka.core.SerializationHelper.read(modelFile);
    }


    private void printInstances(Instances instances) {
        int numAttributes = instances.numAttributes();

        for (int i = 0; i < numAttributes; i++) {
            System.out.println("attribute " + i + " = " + instances.attribute(i));
        }

        System.out.println("class index = " + instances.classIndex());

        int numInstances = instances.numInstances();
        for (int i = 0; i < numInstances; i++) {
            if (i == 5) break;
            Instance instance = instances.instance(i);
            System.out.println("instance = " + instance);
        }
    }

}
