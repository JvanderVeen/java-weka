package nl.bioinf.jlvanderveen;

import java.util.List;

public interface OptionsProvider {
    /**
     * serves the name of the file.
     * @return fileName the file to open
     */
    String getFileName();

    /**
     * serves an Instance.
     * @return singleInstance the instance to classify
     */
    double[] getSingleInstance();
}
