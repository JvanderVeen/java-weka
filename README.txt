Purpose
This project classifies bird bones using the weka library.

Using the application
When running the application for the first use the following command
java -jar WekaRunner-1.0-SNAPSHOT-all.jar -h

This will help understand how to use the application. If you want to use the default values simply run the following command
java -jar WekaRunner-1.0-SNAPSHOT-all.jar

Make sure that the following files are in the same directory as the application
training_data.arff
test_data.arff
stacking_randomforest.model

The compiled application has been added to the root of the project.